#!/bin/sh

TS=`date +%s`

git config --global user.email "paul@morris.so"
git config --global user.name "Paul Morris"

git add upgrade.sh
git commit -m 'update timestamp ${TS}'
git push origin master
